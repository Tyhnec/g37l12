//
//  ViewController.swift
//  G37L12
//AWESOME 4000
//  Created by Ivan Vasilevich on 3/29/16.
//  Copyright © 2016 Ivan Besarab. All rights reserved.
//

import UIKit

let kBgq = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
let kMainQueue = dispatch_get_main_queue()


class ViewController: UIViewController {
    
    
    @IBOutlet weak var imageBox: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        view.backgroundColor = UIColor.greenColor()
        
            }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let url = NSURL(string: "http://speedhunters-wp-production.s3.amazonaws.com/wp-content/uploads/2016/03/25011332/MG_0210.jpg")
        
        dispatch_async(kBgq) {
            let data = NSData(contentsOfURL: url!)
            let picture = UIImage(data: data!)
            dispatch_async(kMainQueue, {
                print("picture downnload done")
                self.imageBox.image = picture
            })

        }
        
        
        
    }
    
}

